#!/bin/bash

# Ægir: this replaces DDEV's pre-start.sh with Ægir workarounds.
#
# @see https://github.com/ddev/ddev/blob/master/containers/ddev-webserver/ddev-webserver-base-scripts/pre-start.sh

# Create a pipe that other processes can use to get info into the Docker logs.
# The normal approach is for the other processes to write to /proc/1/fd/1, but
# that doesn't currently work on Gitpod.
#
# @see https://github.com/gitpod-io/gitpod/issues/17551

set -x
set -eu -o pipefail

logpipe=/var/tmp/logpipe

if [[ ! -p ${logpipe} ]]; then

  # Ægir: for some bizarre reason this ends up existing as a regular file so it
  # must be removed before trying to create it as a pipe.
  if [[ -f ${logpipe} ]]; then
    sudo rm ${logpipe}
  fi

  mkfifo ${logpipe}

fi

# Kill process 1 + process group if this exits or fails.
trap "trap - SIGTERM && kill -- -1" SIGINT SIGTERM EXIT SIGHUP SIGQUIT

cat < ${logpipe}
