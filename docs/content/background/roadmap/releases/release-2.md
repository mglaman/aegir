---
title: Release 2
weight: 20
---

Theme: Stateful [Project](../../concepts#project) support (Drupal)

[Roadmap Request For Comment: Release 2 User stories](https://gitlab.com/groups/aegir/-/epics/2) ([milestone](https://gitlab.com/groups/aegir/-/milestones/2))

* [Start a new Drupal Project](https://gitlab.com/groups/aegir/-/epics/11)
* [Initialize a Drupal Project (VM Deployment Target, Ansible Backend)](https://gitlab.com/groups/aegir/-/epics/12)
* [Host a Project Environment](https://gitlab.com/groups/aegir/-/epics/13)
