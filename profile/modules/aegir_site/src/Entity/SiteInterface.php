<?php

declare(strict_types=1);

namespace Drupal\aegir_site\Entity;

use Drupal\aegir_api\Entity\AegirEntityInterface;
use Drupal\aegir_api\Entity\AegirEntityWithAutoCreateChildrenInterface;

/**
 * Interface for Ægir Site entities.
 */
interface SiteInterface extends AegirEntityInterface, AegirEntityWithAutoCreateChildrenInterface {}
