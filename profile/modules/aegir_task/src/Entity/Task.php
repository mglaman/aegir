<?php

declare(strict_types=1);

namespace Drupal\aegir_task\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

use Drupal\aegir_api\Entity\AegirEntityBase;
use Drupal\aegir_task\Entity\TaskInterface;

/**
 * Defines the Ægir Task entity.
 *
 * @ingroup aegir_task
 *
 * @ContentEntityType(
 *   id               = "aegir_task",
 *   label            = @Translation("Task"),
 *   label_collection = @Translation("Tasks"),
 *   label_singular   = @Translation("task"),
 *   label_plural     = @Translation("tasks"),
 *   label_count      = @PluralTranslation(
 *     singular = "@count task",
 *     plural   = "@count tasks",
 *   ),
 *   bundle_label = @Translation("Task type"),
 *   handlers     = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\AegirEntityListBuilder",
 *     "form"         = {
 *       "add"    = "Drupal\aegir_api\Form\AegirEntityForm",
 *       "edit"   = "Drupal\aegir_api\Form\AegirEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *       "revision-delete" = "Drupal\Core\Entity\Form\RevisionDeleteForm",
 *       "revision-revert" = "Drupal\Core\Entity\Form\RevisionRevertForm",
 *     },
 *     "inline_form"    = "Drupal\aegir_task\Form\TaskInlineForm",
 *     "access"         = "Drupal\aegir_api\Entity\AegirEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html"     = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "revision" = "Drupal\Core\Entity\Routing\RevisionHtmlRouteProvider",
 *     },
 *     "translation"  = "Drupal\content_translation\ContentTranslationHandler",
 *     "views_data"   = "Drupal\views\EntityViewsData",
 *   },
 *   base_table       = "aegir_task",
 *   data_table       = "aegir_task_field_data",
 *   revision_table   = "aegir_task_revision",
 *   revision_data_table = "aegir_task_field_revision",
 *   translatable     = true,
 *   show_revision_ui = true,
 *   admin_permission = "administer aegir task entities",
 *   entity_keys      = {
 *     "id"         = "id",
 *     "revision"   = "revision_id",
 *     "bundle"     = "type",
 *     "label"      = "name",
 *     "uuid"       = "uuid",
 *     "owner"      = "user_id",
 *     "langcode"   = "langcode",
 *     "published"  = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user"        = "revision_user",
 *     "revision_created"     = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical"        = "/admin/aegir/tasks/{aegir_task}",
 *     "add-page"         = "/admin/aegir/tasks/add",
 *     "add-form"         = "/admin/aegir/tasks/add/{aegir_task_type}",
 *     "edit-form"        = "/admin/aegir/tasks/{aegir_task}/edit",
 *     "delete-form"      = "/admin/aegir/tasks/{aegir_task}/delete",
 *     "version-history"  = "/admin/aegir/tasks/{aegir_task}/revisions",
 *     "revision"         = "/admin/aegir/tasks/{aegir_task}/revisions/{aegir_task_revision}/view",
 *     "revision-revert-form" = "/admin/aegir/tasks/{aegir_task}/revisions/{aegir_task_revision}/revert",
 *     "revision-delete-form" = "/admin/aegir/tasks/{aegir_task}/revisions/{aegir_task_revision}/delete",
 *     "collection"       = "/admin/aegir/tasks",
 *   },
 *   bundle_entity_type   = "aegir_task_type",
 *   field_ui_base_route  = "entity.aegir_task_type.canonical",
 * )
 */
class Task extends AegirEntityBase implements TaskInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['task_executor'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Task executor'))
      ->setDescription(t('The backend executor plugin responsible for executing this task.'))
      ->setSettings([
        'max_length' => 32,
        'text_processing' => 0,
      ])
      ->setCardinality(1)
      ->setDefaultValue('ansible')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ])
      ->setRevisionable(TRUE);

    return $fields;
  }

}
