<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\ModalDialog;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\ViewExecutableFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Modal dialog to present an operation log.
 */
class LogModal extends AbstractModal implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  protected ?string $title = 'Operation log';

  /**
   * Constructor; saves dependencies.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @param \Drupal\views\ViewExecutableFactory $viewExecutableFactory
   *   The View executable factory.
   */
  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly ViewExecutableFactory $viewExecutableFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('views.executable'),
    );
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\views\Views::getView()
   *   We do the same thing this does but with full dependency injection rather
   *   than calls to the \Drupal static global.
   *
   * @todo Make the view and display configurable in the plug-in settings.
   */
  protected function getContent(): array {

    /** @var \Drupal\views\ViewEntityInterface|null */
    $viewEntity = $this->entityTypeManager->getStorage('view')->load(
      'task_log',
    );

    /** @var \Drupal\views\ViewExecutable|null */
    $viewExecutable = $this->viewExecutableFactory->get($viewEntity);

    $viewExecutable->setDisplay('task_log_modal');
    $viewExecutable->setArguments([$this->getEntity()->uuid()]);

    return $viewExecutable->render();

  }

}
