<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Plugin\views\field;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Render\ViewsRenderPipelineMarkup;
use Drupal\views\ResultRow;
use Drupal\typed_entity\EntityWrapperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to display an operations status.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("operation_status")
 */
class OperationStatus extends FieldPluginBase {


  /**
   * Constructor; saves dependencies.
   *
   * @param array $configuration
   *   A configuration array containing information about the plug-in instance.
   *
   * @param string $pluginId
   *   The plugin_id for the plug-in instance.
   *
   * @param array $pluginDefinition
   *   The plug-in implementation definition.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    array $configuration, string $pluginId, array $pluginDefinition,
    protected readonly EntityWrapperInterface $typedRepositoryManager,
  ) {

    parent::__construct($configuration, $pluginId, $pluginDefinition);

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition,
  ) {

    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('Drupal\typed_entity\RepositoryManager'),
    );

  }

  /**
   * {@inheritdoc}
   *
   * @TODO Determine whether this is needed.
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // do nothing -- to override the parent query.
    // @TODO Look up the operation status field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // Override the alter text option to always alter the text.
    $options['alter']['contains']['alter_text'] = array('default' => TRUE);
    $options['hide_alter_empty'] = array('default' => FALSE);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // @TODO Provide an option to display text or an icon.

    // Remove the checkbox
    unset($form['alter']['alter_text']);
    unset($form['alter']['text']['#states']);
    unset($form['alter']['help']['#states']);
  }

  /**
   * Return an icon to represent the operation status.
   *
   * @TODO Pass this through a theme function, to allow icon overrides.
   */
  protected function getStatusIcon($status) {
    switch ($status) {
      case 'changed':
      case 'ok':
        $icon = '<i class="fa fa-check-circle"></i>';
        break;
      case 'dispatched':
        $icon = '<div class="three-cogs fa-1x">';
        $icon .= '<i class="fa fa-cog fa-spin fa-2x fa-fw"></i>';
        $icon .= '<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>';
        $icon .= '<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>';
        $icon .= '</div>';
        break;
      case 'warnings':
        $icon = '<i class="fa fa-exclamation-circle"></i>';
        break;
      case 'error':
      case 'failed':
        $icon = '<i class="fa fa-exclamation-triangle"></i>';
        break;
      case 'unknown':
        $icon = '<i class="fa fa-question-circle"></i>';
        break;
      case 'unreachable':
        $icon = '<i class="fa fa-unlink"></i>';
        break;
      case 'none':
        $icon = '<i class="fa fa-play-circle"></i>';
        break;
      default:
        $icon = '<i class="fa fa-question-circle"></i>';
    }
    return '<div class="operation-status-icon operation-status-' . $status . '">' . $icon . '</div>';
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $row) {
    $status = $this->typedRepositoryManager->wrap($this->getEntity($row))->getStatus();
    $this->options['alter']['text'] = $this->getStatusIcon($status);

    // Return some text, so the code never thinks the value is empty.
    return ViewsRenderPipelineMarkup::create(Xss::filterAdmin($this->options['alter']['text']));
  }

}
