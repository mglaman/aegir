<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the Task Log formatter.
 *
 * @FieldFormatter(
 *   id = "operation_status_icon",
 *   label = @Translation("Operation status icon"),
 *   description = @Translation("Formats an operation status as an appropriate icon."),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class OperationStatusIconFormatter extends FormatterBase {

  use StringTranslationTrait;

  /**
   * Constructor; saves dependencies.
   *
   * @param string $pluginId
   *   The plugin_id for the formatter.
   *
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The definition of the field to which the formatter is associated.
   *
   * @param array $settings
   *   The formatter settings.
   *
   * @param string $label
   *   The formatter label display setting.
   *
   * @param string $viewMode
   *   The view mode.
   *
   * @param array $thirdPartySettings
   *   Any third party settings.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(
    string $pluginId,
    array $pluginDefinition,
    FieldDefinitionInterface $fieldDefinition,
    array $settings,
    string $label,
    string $viewMode,
    array $thirdPartySettings,
    protected $stringTranslation,
  ) {

    parent::__construct(
      $pluginId,
      $pluginDefinition,
      $fieldDefinition,
      $settings,
      $label,
      $viewMode,
      $thirdPartySettings,
    );

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition,
  ) {

    return new static(
      $pluginId,
      $pluginDefinition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('string_translation'),
    );

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [$this->t('Displays operation status as an icon.')];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(
    FieldItemListInterface $items, $langcode,
  ): array {
    $elements = [];
    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $elements[$delta] = [
        '#type'     => 'markup',
        '#children' => $this->getStatusIcon($item->value),
      ];
    }

    return $elements;
  }

  /**
   * Return an icon to represent the operation status.
   *
   * @todo Implement this as a template so that it can be filtered for XSS,
   *   customized and/or overridden, and styled via CSS.
   */
  protected function getStatusIcon(string $status): string {
    switch ($status) {
      case 'changed':
      case 'ok':
        $icon = '<i class="fa fa-check-circle"></i>';
        break;
      case 'dispatched':
        $icon = '<div class="three-cogs fa-1x">';
        $icon .= '<i class="fa fa-cog fa-spin fa-2x fa-fw"></i>';
        $icon .= '<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>';
        $icon .= '<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>';
        $icon .= '</div>';
        break;
      case 'warnings':
        $icon = '<i class="fa fa-exclamation-circle"></i>';
        break;
      case 'failed':
        $icon = '<i class="fa fa-exclamation-triangle"></i>';
        break;
      case 'unreachable':
        $icon = '<i class="fa fa-unlink"></i>';
        break;
      case 'none':
        $icon = '<i class="fa fa-caret-square-o-right"></i>';
        break;
      case 'unknown':
      default:
        $icon = '<i class="fa fa-question-circle"></i>';
    }
    return '<div class="operation-status-icon operation-status-' . $status . '">' . $icon . '</div>';
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL): array {
    $elements = parent::view($items, $langcode);
    $elements['#attached']['library'][] = 'aegir_operation/operation_views';
    return $elements;
  }

}
