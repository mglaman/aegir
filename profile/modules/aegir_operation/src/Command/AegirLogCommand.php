<?php

declare(strict_types=1);

namespace Drupal\aegir_operation\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\typed_entity\EntityWrapperInterface;
use function is_null;
use function stripslashes;

/**
 * Class AegirLogCommand.
 *
 * @package Drupal\aegir_log_command
 */
#[AsCommand(
  name:         'aegir:log',
  description:  'Log data into an Ægir Operation.',
)]
class AegirLogCommand extends Command {

  use StringTranslationTrait;

  // The entity type for Aegir operations. We should only be logging to operations.
  const OPERATION_ENTITY_TYPE = 'aegir_operation';

  /**
   * Constructs a new AegirInputCommand object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   *
   * @param \Drupal\typed_entity\EntityWrapperInterface $typedRepositoryManager
   *   The Typed Entity repository manager.
   */
  public function __construct(
    protected readonly EntityRepositoryInterface $entityRepository,
    TranslationInterface $stringTranslation,
    protected readonly EntityWrapperInterface $typedRepositoryManager,
  ) {

    // StringTranslationTrait will use \Drupal::service('string_translation') to
    // fetch the service if it isn't set to the property, so by setting it here
    // we make use of dependency injection best practices.
    $this->setStringTranslation($stringTranslation);

    parent::__construct();

  }

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    $this
      ->setHidden(true)
      ->addArgument(
        'uuid',
        InputArgument::REQUIRED,
        (string) $this->t(
          'The UUID of the entity into which we will be inputting data.',
        ),
      )
      ->addArgument(
        'timestamp',
        InputArgument::REQUIRED,
        (string) $this->t('The unix timestamp of the log line.'),
      )
      ->addArgument(
        'data',
        InputArgument::REQUIRED,
        (string) $this->t('The log data.'),
      )
      ->addArgument(
        'sequence',
        InputArgument::REQUIRED,
        (string) $this->t('The sequence number (deprecated).'),
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(
    InputInterface $input, OutputInterface $output,
  ): int {
    $io = new SymfonyStyle($input, $output);
    $args = $input->getArguments();
    foreach ($args as $key => $value) {
      $io->info($key . ': ' . $value);
    }

    $operation = $this->entityRepository->loadEntityByUuid(self::OPERATION_ENTITY_TYPE, $args['uuid']);

    if (is_null($operation)) {
      $io->error((string) $this->t(
        'No operation found with UUID @uuid',
        ['@uuid' => $args['uuid']],
      ));

      return Command::FAILURE;

    }

    /** @var \Drupal\aegir_operation\WrappedEntities\OperationInterface */
    $wrappedEntity = $this->typedRepositoryManager->wrap($operation);

    $wrappedEntity->addTaskLogOutput(stripslashes($args['data']));
    $wrappedEntity->addTaskLogSequence((int) $args['sequence']);
    $wrappedEntity->addTaskLogTimestamp((int) $args['timestamp']);

    // Don't create a new revision for each line of the log output.
    $wrappedEntity->getEntity()->setNewRevision(false);
    $wrappedEntity->save();

    return Command::SUCCESS;

  }

}
