@deployment_target @access @api
Feature: Access to Aegir deployment targets and bundles
  In order to define deployment targets on which will run applications
  as a deployment target manager,
  I need to be able to access Aegir deployment target entities and bundles.

  Scenario: Anonymous users should not have access to deployment target entities and bundles.
    Given I am not logged in
     When I am on "admin/aegir/deployment-targets/types"
     Then I should be on "user/login"
     Then I should see "Forgot your password?"

  Scenario: Authenticated users should not have access to Deployment Target entities and bundles configuration.
    Given I am logged in as an "Authenticated user"
     When I am on "admin/aegir/deployment-targets/types"
     Then I should not see "Deployment Target types"
      And I should see "Access denied"
      And I should see "You are not authorized to access this page."

  Scenario Outline: Only authorized roles can access Deployment Target entity and bundle configurations.
    Given I am logged in as a "<ROLE>"
     When I am on "admin/aegir/deployment-targets/types"
     Then I should not see "<SHOULD_NOT_SEE>"
      And I should see "<SHOULD_SEE>"
      And I should get a "<HTTP_CODE>" HTTP response
  Examples:
  | ROLE                    | SHOULD_SEE                                 | SHOULD_NOT_SEE                             | HTTP_CODE |
  | administrator           | Deployment Target types                      | You are not authorized to access this page.| 200       |
  | aegir_administrator     | Deployment Target types                      | You are not authorized to access this page.| 200       |
  | aegir_operation_manager | You are not authorized to access this page.| Deployment Target types                      | 403       |
  | aegir_task_manager      | You are not authorized to access this page.| Deployment Target types                      | 403       |
  | aegir_deployment_target_manager         | Deployment Target types                      | You are not authorized to access this page.| 200       |
