<?php

declare(strict_types=1);

namespace Drupal\aegir_example\Entity;

use Drupal\aegir_api\Entity\AegirEntityTypeBase;
use Drupal\aegir_example\Entity\ExampleTypeInterface;

/**
 * Defines the Ægir Example type configuration entity.
 *
 * @ConfigEntityType(
 *   id               = "aegir_example_type",
 *   label            = @Translation("Example type"),
 *   label_collection = @Translation("Example types"),
 *   label_singular   = @Translation("example type"),
 *   label_plural     = @Translation("example types"),
 *   label_count      = @PluralTranslation(
 *     singular = "@count example type",
 *     plural   = "@count example types",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\AegirEntityTypeListBuilder",
 *     "form"         = {
 *       "add"    = "Drupal\aegir_api\Form\AegirEntityTypeForm",
 *       "edit"   = "Drupal\aegir_api\Form\AegirEntityTypeForm",
 *       "delete" = "Drupal\aegir_api\Form\AegirEntityTypeDeleteConfirmForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix    = "aegir_example_type",
 *   admin_permission = "administer aegir example types",
 *   bundle_of        = "aegir_example",
 *   entity_keys = {
 *     "id"     = "id",
 *     "label"  = "label",
 *     "uuid"   = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 *   links = {
 *     "canonical"    = "/admin/aegir/examples/types/{aegir_example_type}",
 *     "add-form"     = "/admin/aegir/examples/types/add",
 *     "edit-form"    = "/admin/aegir/examples/types/{aegir_example_type}/edit",
 *     "delete-form"  = "/admin/aegir/examples/types/{aegir_example_type}/delete",
 *     "collection"   = "/admin/aegir/examples/types",
 *   },
 * )
 */
class ExampleType extends AegirEntityTypeBase implements ExampleTypeInterface {}
