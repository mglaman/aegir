<?php

declare(strict_types=1);

namespace Drupal\aegir_api\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\StyleInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Trait IoTrait.
 *
 * @package Drupal\aegir_api
 */
trait IoTrait {

  /**
   * The input/output instance.
   *
   * @var \Symfony\Component\Console\Style\StyleInterface|Symfony\Component\Console\Output\OutputInterface
   */
  protected StyleInterface|OutputInterface $io;

  /**
   * The Symfony input class.
   *
   * @var \Symfony\Component\Console\Input\InputInterface
   */
  protected InputInterface $input;

  /**
   * The Symfony output class.
   *
   * @var \Symfony\Component\Console\Output\OutputInterface
   */
  protected OutputInterface $output;

  /**
   * Initialize input/output.
   *
   * This was previously provided by Drupal Console before that was removed.
   * Expected to be called by the command initialize() function.
   *
   * @see \Symfony\Component\Console\Command\Command::initialize()
   */
  protected function initializeIo(
    InputInterface $input, OutputInterface $output,
  ): void {

    $this->input = $input;

    $this->output = $output;

    $this->io = new SymfonyStyle($input, $output);

  }

  /**
   * Alias for getInput().
   */
  protected function input(): InputInterface {
    return $this->input;
  }

  /**
   * Alias for $this->io.
   */
  protected function io(): StyleInterface|OutputInterface {
    return $this->io;
  }

}
